{% assign memcat = '' %}

<aside class="mod rightCol">
    <ul class="menu">
    
    {% for cat in (page.sidebar_categories) %}
    
        <li id="{{ cat }}">
        
            <a href="#{{ cat }}">{{ cat | replace:'_',' ' | capitalize }}</a>
            
            <ul>
        {% for post in site.posts reversed %}
        
            {% if post.categories contains 'photon-cloud' %}
            {% if post.categories contains cat %}
            
                {% if post.subcategory %}
                    {% if post.subcategory != memcat %}
                        {% capture memcat %}{{ post.subcategory }}{% endcapture %}
                        
                        <li class="hasChildMenu" id="{{ memcat }}">
        
                            <a href="#{{ memcat }}">{{ memcat | replace:'_',' ' }}</a>
                            
                            <ul>
                            
                        {% for post in site.posts reversed %}
                            {% if post.subcategory %}
                                {% if post.subcategory == memcat %}
                                
                                    {% if page.title == post.title %}
                                        {% assign activeCssClass = 'active' %}
                                    {% else %}
                                        {% assign activeCssClass = '' %}
                                    {% endif %}
                                
                                    <li class="{{ activeCssClass }}{{ addCssClass }}">
                                        <a href="{{ post.url }}{{ GET }}#cat-{{ memcat }}">{{ post.title }}</a>
                                    </li>
                                
                                {% endif %}
                            {% endif%}
                        {% endfor %}
                        
                        </ul>
                        
                    {% endif%}
                {% else %}
                
                    {% if page.title == post.title %}
                        {% assign activeCssClass = 'active' %}
                    {% else %}
                        {% assign activeCssClass = '' %}
                    {% endif %}
                    
                    <li class="{{ activeCssClass }}{{ addCssClass }}" data-postcategory="{{ curPostCategory }}">
                        <a href="{{ post.url }}{{ GET }}#cat-{{ cat }}">{{ post.title }}</a>
                    </li>
                
                {% endif %}
                
            {% endif %}
            {% endif %}
            
        {% endfor %}
         
            </ul>
        </li>
            
    {% endfor %}
    
    </ul>
</aside>