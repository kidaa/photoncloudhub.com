---
layout: article
title: Binary Protocol
categories: [photon-server, photon-cloud, references]
tags: [overview, how-to]
---
{% include globals %}

Photon and its clients are using a highly optimized binary protocol to
communicate. It's compact, yet easy to parse. You don't have to know its
details, as it is handled behind the scenes. Just in case you want to
know it anyways, this page describes it.

## Communication Layers

The Photon binary protocols are organized in several layers. On its
lowest level, UDP is used to carry the messages you want to send. Within
those standard datagrams, several headers reflect the properties you
expect of Photon: optional reliability, sequencing, aggregation of
messages, time synchronization and various others.

The following chart shows the individual layers and their nesting:

<figure>
<img src="{{ IMG }}/BinaryProtocol-udp-layers.png" />
<figcaption>Layers of the binary protocol</figcaption>
</figure>

In words: Any UDP package contains an Enet header and at least one enet
command. Each enet command carries our messages: An Operation, result or
event. Those in turn consist of the operation header and the data you
provided.

Below, you will find tables listing each part of the protocols and it's
size. This should give you an idea of the bandwidth needed by certain
content you send. If you wanted to, you could calculate how big any
given message becomes.

To save you this work, we have an example, too.

## Example: Join "somegame"

Let's take a look at the operation join (implemented by the Lite
Application). Without properties, this is an operation with a single
parameter.

The "operation parameters" require: 2 + count(parameter-keys) +
size(parameter-values) bytes. The string "somegame" uses 8 bytes in UTF8
encoding and strings require another 3 bytes (length and type
information). **Sum: 14 bytes.**

The parameters are wrapped into an operation-header, which is 8 bytes.
**Sum: 22 bytes.**

The operation code is not encoded as parameter. Instead it is in the
operation-header.

The operation and its header are wrapped into Enet's "send reliable"
command as payload. The command is 12 bytes. **Sum: 34 bytes.**

Let us assume the command is sent when no other commands are queued. The
Enet package header is 12 bytes. **Sum: 46 bytes for the reliable,
sequenced operation.**

Last but not least, the Enet package is put into a UDP/IP datagram,
which adds 28 bytes headers. Quite a bit, compared to the 46 bytes so
far. Sadly, this can't be avoided completely but our aggregation of
commands can save you a lot of bandwidth, sharing those headers.

**The complete operation takes up 74 bytes.**

The server will have to acknowledge this command. The ACK command is 20
bytes. If this is sent alone in a package, it will take up 40 bytes in
return.

<figure>
<img src="{{ IMG }}/BinaryProtocol-HexBytes.jpg" />
<figcaption>Join "somegame" in Wireshark</figcaption>
</figure>

## Operation Content - Serializable Types
<table class="list"><tbody>
<tr>
<th> type (C#)</th>
<th> size [bytes]</th>
<th> description</th>
</tr>
<tr>
<td> byte <br> </td>
<td> 2 <br> </td>

<td> 8 bit <br> </td>
</tr>
<tr>
<td> boolean <br> </td>
<td> 2 <br> </td>
<td> true or false <br> </td>

</tr>
<tr>
<td> short <br> </td>
<td> 3 <br> </td>
<td> 16 bit <br> </td>
</tr>
<tr>
<td> int <br> </td>

<td> 5 <br> </td>
<td> 32 bit <br> </td>
</tr>
<tr>
<td> long <br> </td>
<td> 9 <br> </td>

<td> 64 bit <br> </td>
</tr>
<tr>
<td> float <br> </td>
<td> 5 <br> </td>
<td> 32 bit <br> </td>

</tr>
<tr>
<td> double <br> </td>
<td> 9 <br> </td>
<td> 64 bit <br> </td>
</tr>
<tr>
<td> string <br> </td>

<td> 3 + size( UTF8.GetBytes(string) ) <br> </td>
<td>&lt; short.MaxValue length</td>
</tr>

<tr>
<td> byte-array <br> </td>
<td> 5 + 1 * length <br> </td>

<td>&lt; int.MaxValue length</td>
</tr>

<tr>
<td> int-array <br> </td>
<td> 5 + 4 * length <br> </td>

<td>&lt; int.MaxValue length</td>
</tr>

<tr>
<td> array of &lt;type&gt; <br> </td>
<td> 4 + size(entries) - count(entries) <br> </td>
<td>&lt; short.MaxValue length</td>

</tr>
<tr>
<td> hashtable <br> </td>
<td> 3 + size(keys) + size(values) <br> </td>
<td>&lt; short.MaxValue pairs</td>
</tr>
<tr>
<td> operation parameters <br>

<br> </td>
<td> 2 + count(parameter-keys) + size(parameter-values) <br> </td>
<td>&lt; short.MaxValue pairs<br>
special case of a hashtable - see below</td>
</tr>

</tbody></table>


<p>On the client side, operation parameters and their values are aggregated within a Hashtable. As each parameter is resembled by a byte key, operation requests are streamlined and use less bytes than any "regular" hashtable.</p>

<p>Results for operations and events are encoded in the same way as operation parameters. An operation result will always contain: opCode, returnCode and a debug string. Events always contain: evCode, actorNr and the event data Hashtable.</p>

<h2>Enet Commands</h2>


<table class="list"><tbody>
<tr>
<th> name </th>
<th> size</th>
<th> sent&nbsp;by</th>
<th> description</th>
</tr>
<tr>
<td> connect <br> </td>
<td> 44 <br> </td>
<td> client <br> </td>

<td> reliable, per connection<br> </td>
</tr>
<tr>
<td> verify connect <br> </td>
<td> 44 <br> </td>
<td> server <br> </td>

<td> reliable, per connect<br> </td>
</tr>
<tr>
<td> init message <br> </td>
<td> 57 <br> </td>
<td> client <br> </td>

<td> reliable, per connection (choses the application)<br> </td>
</tr>
<tr>
<td> init response <br> </td>
<td> 19 <br> </td>
<td> server <br> </td>

<td> reliable, per init<br> </td>
</tr>
<tr>
<td> ping <br> </td>
<td> 12 <br> </td>
<td> both <br> </td>

<td> reliable, called in intervals (if nothing else was reliable)<br> </td>
</tr>
<tr>
<td> fetch timestamp <br> </td>
<td> 12 <br> </td>
<td> client <br> </td>

<td> a ping which is immediately answered <br> </td>
</tr>
<tr>
<td> ack <br> </td>
<td> 20 <br> </td>
<td> both <br> </td>

<td> unreliable, per reliable command<br> </td>
</tr>
<tr>
<td> disconnect <br> </td>
<td> 12 <br> </td>
<td> both <br> </td>

<td> reliable, might be unreliable in case of timeout<br> </td>
</tr>
<tr>
<td> send reliable <br> </td>
<td> 12 + payload <br> </td>
<td> both <br> </td>

<td> reliable, carries a operation, response or event<br> </td>
</tr>
<tr>
<td> send unreliable <br> </td>
<td> 16 + payload <br> </td>
<td> both <br> </td>

<td> unreliable, carries a operation, response or event<br> </td>
</tr>
<tr>
<td> fragment <br> </td>
<td> 32 + payload <br> </td>
<td> both <br> </td>

<td> reliable, used if the payload does not fit into a single datagram<br> </td>
</tr>
</tbody></table>



<h2>UDP Packet Content - Headers</h2>

<table class="list"><tbody>
<tr>
<th> name </th>
<th> size [bytes]</th>
<th> description</th>
</tr>
<tr>
<td> udp/ip <br> </td>
<td> 28 + size(optional headers) <br> </td>
<td> IP + UDP Header. <br>

Optional headers are less than 40 bytes. <br> </td>
</tr>
<tr>
<td> enet packet header <br> </td>
<td> 12 <br> </td>
<td> contains up to byte.MaxValue commands <br>
(see above) <br> </td>

</tr>
<tr>
<td> operation header <br> </td>
<td> 8 <br> </td>
<td> in a command (reliable, unreliable or fragment) <br>
contains operation parameters <br>
(see above) <br> </td>

</tr>
</tbody></table>