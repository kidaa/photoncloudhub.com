---
layout: article
title: Feature Overview
categories: [photon-cloud, getting_started]
tags: [overview, quickstart]
---
{% include globals %}

Let us start with a quick overview of Photon Cloud. Out of the box it features an array of useful 
functions and operations.

## Pick the Cloud API
The first decision you have to take is picking one of the two Cloud APIs: The "pure API" or the 
"PUN API". PUN stands for Photon Unity Networking and aonly available for Unity3d.
<p>
<figure>
<img style="border: none;" src="{{ IMG }}/CloudOverview_1.png" />
<figcaption>Cloud API Options.</figcaption>
</figure>
</p>
You can download the according SDKs here

- <strong>pure API</strong> in the [download section](http://cloud.exitgames.com/Download)
- <strong>PUN API</strong> in the [Unity Asset Store](http://u3d.as/content/exit-games/photon-unity-networking-free/2ey)

## #1: Features of the pure API
The pure API offers simple but powerful functionalities for <strong>matchmaking</strong> and <strong>data exchange</strong>
that is used for realtime gameplay.

### Matchmaking
Brings players together in games/rooms based on criteria you (or the players :) decide.

<ul class="mod noBullets bgMedium rC4">
    <li><b>Random:</b> Allow to quick match players into a room with a free slot.</li>
    <li><b>Room based:</b> Players join a specified room through the room name.</li>
    <li><b>Private:</b> Room is not public visible - only invited players can join.</li>
    <li><b>Parameterized:</b> Filter the room list based on its properties.</li>
</ul>

**Operations** used are Connect(), Disconnect(), CreateRoom(), JoinRandom(), JoinRoom(), Leave()

###Realtime Gameplay, Data Exchange
This is where the gameplay happens - exchange vital data like position in a FPS or turn information in turn-based games.
Data is sent near realtime via TCP, Websockets or UDP (reliable or unreliable).

<ul class="mod noBullets bgMedium rC4">
    <li><b>Send messages/events:</b> Send any type of data to other players.</li>
    <li><b>Player/Room properties:</b> Set properties of players and rooms.</li>
</ul>

**Operations** used are RaiseEvent(), SetProperties(), GetProperties()

If you want to use custom server logic or need authorative server control you should check out our Photon Server product. 
Nearly all the code you write for a Photon Cloud client can be used with Photon Server too! 

However, there are ways to complement Photon Cloud with various means to achieve your vision.
We'll give you some examples on how successful games managed to use Photon Cloud to their advantage.

## #2: Features of the PUN API
Unity Networking (or short UN) is the embedded networking technology from Unity (see [Unity Networking](http://unity3d.com/unity/engine/networking).

Unity Networking is

* very convenient, simple and powerful
* the entry point for most Indies to start networking


But Unity Networking has some serious draw backs

+ It needs a Master Server to be self-hosted
+ The peer-2-peer tech used leads to NAT/punch-through issues (especially in mobile networks)
+ A dropped host kills the game and so frustrates players
+ It will probably never run on the new Unity export targets like Flash or Chrme NaCl


<strong>PUN fixes all issues from Unity Networking and extends it.</strong>

- It is <strong>almost API compatible</strong> and allows an easy migration
- PUN <strong>runs in the cloud</strong> and needs no additional Master Server
- PUN has a client-2-server infrastructure and is <strong>reliabale to connect</strong> and supports high bandwidths
- PUN <strong>scales unlimited</strong>: Some applications already hit 10,000 CCUs and more

Here the feature set that is supported compared to Unity Networking:
<p>
<figure>
<img style="border: none;" src="{{ IMG }}/pun_features.png" />
</figure>
</p>

Here the additionl features that are on top of Unity Networking:
<p>
<figure>
<img style="border: none;" src="{{ IMG }}/pun_additional.png" />
</figure>
</p>

## References

->BANNERS + BULLETPOINT KEY FACTS HERE<-