---
layout: article
title: First Steps
categories: [photon-cloud, getting_started]
tags: [overview, quickstart, how-to, setup, installation]
---
{% include globals %}

If you didn't already register on our site you can do so [here](http://cloud.exitgames.com/Account/SignUp).
After the registration you will find an active 30 day trial in your [application dashboard](http://cloud.exitgames.com/Dashboard). There you will also find the Cloud App ID you use to connect to the cloud service.

### Registration through Unity 

The Unity package "Photon Unity Networking" found at the Unity Store includes a setup wizard,
which opens automatically when you import the plugin. Simply submit your
email address in the Photon Unity Networking window to generate your
account and Cloud App ID. It is effective immediately. Done.

If your email is already connected with an account, you can visit out
webpage to get your Cloud App ID. We will mail you the credentials to
log-in, then you can set a password and access your Cloud App ID.

### Photon Cloud App ID

The Cloud App ID is a generated identifier for your Photon Cloud
application. It's used when an application client connects and separates
your users from anyone else's.

### Next Steps

Once you have your own Cloud App ID, you should use it within the
provided samples. Just [download the SDK](http://cloud.exitgames.com/Download) for your prefered system and check the provided samples.
The Load Balancing Demo is a good way to start your first steps. Luckily we have a [quick roundup](/LoadBalancingDemo) of what you can do with the demo featuring actual code samples (in C#).


### Application logic

Currently, you can not modify the server's logic within the cloud. The
flexible "Loadbalancing" framework should enable you to build any
room-based realtime (and turnbased) game. The server simply forwards all
RPCs and synchronization events between your clients.

### Self hosting

As alternative to the Photon Cloud Service, you can download the [Photon
Server SDK](http://www.exitgames.com/Download/Photon). It includes the
binaries to run a server on [suitable Windows machines](http://doc-server.exitgames.com/Requirements#cat-getting_started) and the source to
modify the server's logic.

Switching to your own server should be simple, as the basic workflow and
protocol stays intact. The Cloud App ID is no longer used and you need
your own Photon license. Also, you have to setup your server machine.
